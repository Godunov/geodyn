////////////////////////////////////////
//	HURRICANE / STORM HANDLING
//	Hurricane data from the NOAA
//	http://www.ncdc.noaa.gov/oa/ibtracs/index.php?name=ibtracs-data
//	with custom python script to turn their csv into json...
//	Using Tween.js
////////////////////////////////////////

var origin = new THREE.Vector3(0,0,0);
var hurricanesReady = false;
var hurricanesEnabled = false;
var hurricaneData;

var trackerMesh = [];

TWEEN.start();

function resetAllMarkers(){
	//	reset hurricane time indicies
	for( var i=0; i<hurricaneData.length; i++ ){
		var hurricane = hurricaneData[i];
		hurricane.index = 0;
		if ( hurricane.display !== undefined ) hurricane.display.hide();
	}
}		

function loadHurricaneData(){
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if ( xhr.readyState == 4 ) {
			if ( xhr.status == 200 || xhr.status == 0 ) {
				hurricaneData = JSON.parse( xhr.responseText );
				// console.log(hurricaneData)	
				// for( var i in hurricaneData ){
				// 	var hur = hurricaneData[i];
				// 	if( hur.name == 'ONE' ){
				// 		console.log( hur );
				// 		break;
				// 	}
				// }

				loadSVGAsset('images/hurricanetarget.svg', function(svg){
					initHurricanes();				
				});			
			}
		}
	}
	xhr.open( 'GET', 'hurricanes.json', true );
	xhr.overrideMimeType( 'text/plain; charset=x-user-defined' );
	xhr.setRequestHeader( 'Content-Type', 'text/plain' );
	xhr.send( null );	
}


function initHurricanes(){
	var geo = new THREE.CubeGeometry(40, 40, 40, 1, 1, 1);
	var mat = new THREE.MeshBasicMaterial( {wireframe:true} );
	var container = document.getElementById("iffcontainer");
	var _this = this;
	for( var i=0; i<hurricaneData.length; i++ ) {
		var mesh = new THREE.Mesh(geo, mat);
		mesh.use = false;
		mesh.visible = false;
		trackerMesh.push( mesh )
		trackerMesh[i].position = geoToSpace( hurricaneData[i].data[0].lat, hurricaneData[i].data[0].lon, 150);

		globeObject.add( mesh );	
		hurricaneData[i].index = 0;			

		// trackerMesh[i].visible = false;

		// if( hurricaneData[i].end < timeline.start  ){			
		// 	continue;
		// }

		var toy = new SVGToy( 'images/hurricanetarget.svg', 
			function( svgToy ){			
				hurricaneData[i].display = svgToy;							
				svgToy.setText('TextName', hurricaneData[i].name );
				svgToy.setPosition( - 1000, -1000 );				
				svgToy.setProperty('Context','visibility','hidden');							
			},
		container );
		
		hurricaneData[i].scale = 5.0;		
	}
	hurricanesReady = true;	
}

var r = 0;
function checkHurricanes(){
	if( hurricanesReady == false )
		return;

	if( hurricanesEnabled == false )
		return;	

	// r += 5;	
	// if( r > 360 )
	// 	r -= 360;

	var centerX = window.innerWidth / 2;
	var centerY = window.innerHeight / 2;

	// var camR = rotation.x * 180 / Math.PI;
	// // console.log(camR);
	for( var i=0; i<hurricaneData.length; i++ ){
		var hurricane = hurricaneData[i];

		var mesh = trackerMesh[i];
		var meshAbs = undefined;		

		var data = hurricane.data[hurricane.index];

		if( data === undefined ){
			continue;
		}

		if( hurricane.start < timeline.start || hurricane.end < timeline.start )
			continue;

		if( timeline.cursor >= hurricane.start && timeline.cursor <= hurricane.end ){
			// console.log(timeline.start,timeline.cursor,timeline.end);
			// console.log(hurricane.name,hurricane.start,hurricane.end);
			// console.log("cursor:"+timeline.cursor+">="+hurricane.start+" && "+"cursor:"+timeline.cursor+"<="+hurricane.end);
			mesh.use = true;
			// mesh.visible = true;

			while( data.time < timeline.cursor ){
				hurricane.index += 1;
				data = hurricane.data[hurricane.index];
				if( data === undefined ){
					continue;
				}
			}
			
			//	set the mesh 3d position
			mesh.targetPosition = geoToSpace( data.lat, data.lon, 150 );
			mesh.position.x += (mesh.targetPosition.x - mesh.position.x) * 0.1;
			mesh.position.y += (mesh.targetPosition.y - mesh.position.y) * 0.1;
			mesh.position.z += (mesh.targetPosition.z - mesh.position.z) * 0.1;
			mesh.lookAt( origin );	
		}
		else{
			mesh.use = false;
			// mesh.visible = false;
		}

		if( mesh.use ){
			meshAbs = getAbsOrigin( mesh );							
			var screenPos = screenXY( meshAbs );
			hurricane.display.setPosition( screenPos.x, screenPos.y );			
			
			if( meshAbs.z > 0 )
				hurricane.display.show();
			else
				hurricane.display.hide();

			var opacity = meshAbs.z;
			if( opacity > 100 )
				opacity = 100;
			if( opacity < 0 )
				opacity = 0;
			opacity *= 0.01;

			hurricane.display.setAlpha(opacity);
			hurricane.display.setZIndex( 100 + meshAbs.z * 100);

			var hcat = getHurricaneCategory( data.wind );			
			var name =  hurricane.name;
			if( data.nature != '' ){
				name += ' (' + data.nature +')';
			}
			hurricane.display.setText('TextName', name );

			var svgScreenPos = {};
			svgScreenPos.x = (screenPos.x + hurricane.display.origin.x);
			svgScreenPos.y = (screenPos.y + hurricane.display.origin.y);

			var deltaVector = {};
			deltaVector.x =  screenPos.x - centerX;
			deltaVector.y =  screenPos.y - centerY + 80;

			//	normalize
			var dist = Math.sqrt( deltaVector.x * deltaVector.x + deltaVector.y * deltaVector.y );
			deltaVector.x /= dist;
			deltaVector.y /= dist;			

			//	push out
			deltaVector.x *= 70;
			deltaVector.y *= 70;			

			var anchor = {};
			anchor.x = hurricane.display.origin.x + deltaVector.x;
			anchor.y = hurricane.display.origin.y + deltaVector.y;

			var basePoint = {};
			basePoint.x = anchor.x + deltaVector.x;
			basePoint.y = anchor.y + deltaVector.y;

			hurricane.display.setProperty('pointer', 'x2', (anchor.x) + '' );
			hurricane.display.setProperty('pointer', 'y2', (anchor.y) + '' );

			hurricane.display.setProperty('pointer', 'x1', (basePoint.x) + '' );
			hurricane.display.setProperty('pointer', 'y1', (basePoint.y) + '' );			

			var right = (centerX < screenPos.x) ? 1 : -1;		

			hurricane.display.setProperty('underpath', 'x1', (basePoint.x) + '' );
			hurricane.display.setProperty('underpath', 'y1', (basePoint.y) + '' );				
			hurricane.display.setProperty('underpath', 'x2', (basePoint.x + 400 * right) + '' );
			hurricane.display.setProperty('underpath', 'y2', (basePoint.y) + '' );		

			var textX = basePoint.x;
			if( right < 0 )
				textX -= 400;
			
			var textY = basePoint.y - 40;
			var value = hurricane.display.beginTransform()
			.translate( textX, textY)
			.endTransform();
			hurricane.display.setProperty( 'Infotext', 'transform', value );		

			// console.log(hurricane.display.origin);

			var windtext = data.wind + ' miles per hour';
			if( hcat > 0 )
				windtext += ' (Category ' + hcat + ')';
			hurricane.display.setText('TextWind', windtext )			
			// if( opacity >= 1.0){
				var clamper = hurricane.display.svg.getElementById('Context');
				clamper.style.visibility = 'visible';
			// }
			// else{
			// 	var clamper = hurricane.display.svg.getElementById('Context');
			// 	clamper.style.visibility = 'hidden';
			// }

		}
		else{
			if( hurricane.display ){
				hurricane.display.hide();
				var clamper = hurricane.display.svg.getElementById('Context');
				clamper.style.visibility = 'hidden';
			}
			
		}
		
	}

}

//	Ssaffir-Simpson Hurricane Wind Scale
//	http://www.nhc.noaa.gov/aboutsshws.php
function getHurricaneCategory( windMPH ){
	if( windMPH < 74 )
		return 0;
	if( windMPH >= 74 && windMPH <= 95 )
		return 1;
	if( windMPH >= 96 && windMPH <= 110 )
		return 2;
	if( windMPH >= 111 && windMPH <= 129 )
		return 3;
	if( windMPH >= 130 && windMPH <= 156 )
		return 4;
	if( windMPH >= 157 )
		return 5;
}

function getAbsOrigin( object3D ){
	return object3D.matrixWorld.getPosition();
}

function geoToSpace(lat, lng, rad) {
    var phi = (90 - lat) * Math.PI / 180;
    var theta = (180 - lng) * Math.PI / 180 - Math.PI ;

    var position = new THREE.Vector3();
    position.x = rad * Math.sin(phi) * Math.cos(theta);
    position.y = rad * Math.cos(phi);
    position.z = rad * Math.sin(phi) * Math.sin(theta);

    return position;
}

var projector = new THREE.Projector();

function screenXY(vec3){
	var vector = projector.projectVector( vec3.clone(), camera );
	// console.log(vector);
	var result = new Object();
	result.x = Math.round( vector.x * (window.innerWidth/2) ) + window.innerWidth/2;
	result.y = Math.round( (0-vector.y) * (window.innerHeight/2) ) + window.innerHeight/2;
	return result;
}

