﻿////////////////////////////////////////
//	Main
//	Globe visualization using THREE.js
////////////////////////////////////////

var container = document.getElementById( 'container' );
var camera, scene, renderer, effect;
var textures;
var globeObject;
var texture;
var globeMaterial;
var cloudMaterial;
var cloudGeo;
var timeline;
var video;
var quality = 'low';
var level = 9;
var year_segments;
var current_year_no;
var level_no;
var s362d_level_no = 9;
var s362d_isosurfaces;
var s362d_video;
var s362d_texture;
var s362d_video_source = 'geodyn/s362d/deeps.webm';
var s362d_mesh;

var VIDEO_SOURCE_GEOMAGNETIC = 'geodyn/';
var VIDEO_SOURCE_GEOMAGNETIC_DIFFS = 'geodyn/diffs/';
var video_source = VIDEO_SOURCE_GEOMAGNETIC;

function updateVideoSource(){
    var currentTime = video.currentTime;
    var loadStart = function(event){
        video.currentTime = currentTime;
    }

	video.src = video_source+(level+1)+'.webm';
	if (show_magneticfield) {
		globeObject.add( meshes[level] );
	}
    
    video.addEventListener('loadedmetadata', loadStart, false );
    // video.setAttribute( 'crossorigin', 'anonymous' );
	if (pause) {
		video.pause();
	} else {
		video.play();
	}
}

function toggleVideo(_level){
	globeObject.remove( meshes[level] );
	level = _level;
    updateVideoSource();
}

function toggleS362DDeep(value, deep){
	var progress = value/22;
	s362d_video.currentTime = progress * s362d_video.duration;
	
	var scale = (6000.0-deep)/6000.0;
	s362d_mesh.scale.set(scale, scale, scale);
}

var show_map = true;
var map;
function showMap(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	show_map = !show_map;
    
	if (show_map) {
		globeObject.add( map );
	} else {
		globeObject.remove( map );
	}
}

var show_magneticfield = false;
function showMagneticField(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	show_magneticfield = !show_magneticfield;
	
	if (show_magneticfield) {
		globeObject.add( meshes[level] );
	} else {
		globeObject.remove( meshes[level] );
	}
}

var show_s362d = false;
function showS362D(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	show_s362d = !show_s362d;
	
	if (show_s362d) {
		globeObject.add(s362d_mesh);
	} else {
		globeObject.remove(s362d_mesh);
	}
}

var show_segments = false;
function showSegments(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	show_segments = !show_segments;
	
	if (show_segments) {
		globeObject.add(year_segments[current_year_no][level_no]);
	} else {
		globeObject.remove(year_segments[current_year_no][level_no]);
	}
}

function selectFunLevel(level) {
	globeObject.remove(year_segments[current_year_no][level_no]);
	
	level_no = level;
	
	if (show_segments) {
		globeObject.add(year_segments[current_year_no][level_no]);
	}
}

var show_s362d_surfaces = false;
function showS362DSurfaces(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	show_s362d_surfaces = !show_s362d_surfaces;
	
	if (show_s362d_surfaces) {
		globeObject.add(s362d_isosurfaces[s362d_level_no]);
	} else {
		globeObject.remove(s362d_isosurfaces[s362d_level_no]);
	}
}

function selectS362DSurface(level) {
	globeObject.remove(s362d_isosurfaces[s362d_level_no]);
	
	s362d_level_no = level;
	
	if (show_s362d_surfaces) {
		globeObject.add(s362d_isosurfaces[s362d_level_no]);
	}
}

var pause = false;
function pauseVideo(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	pause = !pause;
	
	if (pause) {
		video.pause();
	} else {
		video.play();
	}
}

var isDiffState = false;
function switchDiffState(caller){
    var element = caller;
    element.classList.toggle('selected');
	
	isDiffState = !isDiffState;
	
	if (isDiffState) {
		video_source = VIDEO_SOURCE_GEOMAGNETIC_DIFFS;
	} else {
		video_source = VIDEO_SOURCE_GEOMAGNETIC;
	}
	updateVideoSource();
}

function init() {
	//	detect for webgl and reject everything else
	if ( ! Detector.webgl ) {
		Detector.addGetWebGLMessage();
		hideHelp();
		return;
	}

	//	load hurricane / storm data
//	loadHurricaneData();

	//	load the video
	video = document.createElement('video');
	video.id = 'cloudvideo';

	//	start the video at somewhere interesting during the mass hurricane season
	video.addEventListener( 'loadedmetadata', function(e){
		// video.currentTime = 90;
		video.currentTime = 1;
	}, false);	

	video.src = video_source+(level+1)+'.webm';
	
	video.play();

	video.loop = 'loop';

	//	what?
	//	... this doesnt work but
	// video.crossOrigin = 'anonymous';

	//	this does
	//	......
	// video.setAttribute( 'crossorigin', 'anonymous' );

	//	@*&)(*!$)!(*$)

	//document.getElementById('cloudvideo');

	//	initialize the texture with the video as map
	texture = new THREE.Texture( video );

	//	load the s362d video
	s362d_video = document.createElement('video');
	s362d_video.id = 's362d_video';
	//	start the video at somewhere interesting during the mass hurricane season
	s362d_video.addEventListener( 'loadedmetadata', function(e){
		// video.currentTime = 90;
		s362d_video.currentTime = 1;
	}, false);	
	s362d_video.src = s362d_video_source;
	// s362d_video.play();
	s362d_video.pause();
	s362d_video.loop = 'loop';
	//	initialize the texture with the video as map
	s362d_texture = new THREE.Texture( s362d_video );

	//	do THREE things...
	camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 10000 );
	camera.position.x = 0;
	camera.position.y = embed ? 0 : -20;
	camera.position.z = 1000;

	scene = new THREE.Scene();

	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );

	var width = window.innerWidth || 2;
	var height = window.innerHeight || 2;

	effect = new THREE.AnaglyphEffect( renderer );
	effect.setSize( width, height );

	var windowResize = THREEx.WindowResize(renderer, camera);

	buildScene();

	addControlListeners();

	timeline = new Timeline( video );
	document.body.appendChild( timeline.getDomElement() );

	setInterval( function () {

		if ( video.readyState === video.HAVE_ENOUGH_DATA ) {

			texture.needsUpdate = true;
			timeline.update();

		}

	}, 1000 / 12 );

	setInterval( function () {
		if ( s362d_video.readyState === s362d_video.HAVE_ENOUGH_DATA ) {
			s362d_texture.needsUpdate = true;
		}
	}, 1000 / 12 );

	// console.log("video loading at... " +  Date.now() );

	//	default to storms
	pickCategory(embed ? 'rainforest' : 'hurricanes');	
	
	globeObject.add( map );
	
	var pointLight = new THREE.PointLight(0xFFFFFF);
	// set its position
	pointLight.position.x = 50;
	pointLight.position.y = 50;
	pointLight.position.z = 500;
	// add to the scene
	scene.add(pointLight);
}

var meshes = new Array();
function buildScene(){

	globeObject = new THREE.Object3D();
	scene.add(globeObject);

	for (var i=0; i<11; i++)
	{
		rad = 150*(6000-i*300)/6000;
		meshes[i] = createLevelMesh(texture, rad);
	}
	globeObject.add( meshes[level] );
	
	s362d_mesh = createLevelMesh(s362d_texture, 150.0);
	// globeObject.add( s362d_mesh );
	
	mesh = createBlendedMesh();
//	globeObject.add( mesh );
	cloudGeo = mesh;

	map = createMap(150);
	globeObject.add(createMap2(150));

	year_segments = new Array();
	for (var j=0; j<vertices.length; j++)
	{
		var fun_levels = new Array();
		for (var i=0; i<vertices[j].length; i++)
		{
			fun_levels[i] = createSegment(vertices[j][i], cubeVertexIndices[j][i]);
		}
		year_segments.push(fun_levels);
	}
	
	current_year_no = 0;
	level_no = 3;
	
	levelButtons = '';
	for (var i=0; i<levels.length; i++)
	{
		levelButtons += "<a href=\"#\" onclick=\"selectFunLevel(" + i + ")\" class=\"deepselect\">" + levels[i] + "</a><br/>";
	}
	$('#levelsDiv').html(levelButtons)
	
	// s362d isosurfaces
	s362d_isosurfaces = new Array();
	for (var i=0; i<s362d_vertices.length; i++)
	{
		s362d_isosurfaces.push(createSegment_s362d(s362d_vertices[i], s362d_cubeVertexIndices[i]));
	}
	
	s362d_isosurfaceButtons = '';
	for (var i=0; i<s362d_iso_values.length; i++)
	{
		s362d_isosurfaceButtons += "<a href=\"#\" onclick=\"selectS362DSurface(" + i + ")\" class=\"deepselect\">" + s362d_iso_values[i] + "</a><br/>";
	}
	$('#s362d_isosurfaceDiv').html(s362d_isosurfaceButtons)
	
}

function createLevelMesh(texture, rad) {
	var maxAnisotropy = renderer.getMaxAnisotropy();

	textures['landocean'].anisotropy = maxAnisotropy;
	textures['vegetation'].anisotropy = maxAnisotropy;
	textures['currents'].anisotropy = maxAnisotropy;
//	texture.anisotropy = maxAnisotropy;

	uniformsGlobe = THREE.UniformsUtils.clone( Shaders['earth'].uniforms );
	// uniformsGlobe['textureTerrain'].texture = textures[options.map];
	uniformsGlobe['textureClouds'].texture = texture;
	uniformsGlobe['textureNight'].texture = new THREE.ImageUtils.loadTexture('images/land_lights_bloom.png');
	uniformsGlobe['nightMultiplier'].value = 0.0;
	// uniformsGlobe['cloudShadowAmount'].value = 1.0;
	uniformsGlobe['time'].value = 0.0;

	var globeGeometry = new THREE.SphereGeometry( rad, 40, 40, 0, Math.PI * 2, 0, Math.PI);
	globeMaterial = new THREE.ShaderMaterial({
		uniforms: uniformsGlobe,
		vertexShader: document.getElementById( 'globeVertexShader' ).textContent,
		fragmentShader: document.getElementById( 'globeFragmentShader' ).textContent
	});
	mesh = new THREE.Mesh( globeGeometry, globeMaterial );
	return mesh;
}

function createSegment(vertices, cubeVertexIndices) {
	var geom = new THREE.Geometry(); 
	
	for (var i=0; i<vertices.length/3; i++) {
		geom.vertices.push(new THREE.Vector3(150*vertices[3*i],-150*vertices[3*i+1],-150*vertices[3*i+2]));
	}
	
	for (var i=0; i<cubeVertexIndices.length/3; i++) {
		geom.faces.push( new THREE.Face3( cubeVertexIndices[3*i], cubeVertexIndices[3*i+2], cubeVertexIndices[3*i+1] ) );
	}
	
	geom.computeFaceNormals();
	
	var object = new THREE.Mesh( geom, new THREE.MeshNormalMaterial() );
	// var object = new THREE.Mesh( geom, new THREE.MeshNormalMaterial({ opacity: 0.3 }) );
	
	object.rotation.x = Math.PI/2;
	object.rotation.y = -Math.PI;
	object.rotation.z = -Math.PI/2;
	
	return object;
}

function createSegment_s362d(vertices, cubeVertexIndices) {
	var geom = new THREE.Geometry(); 
	
	for (var i=0; i<vertices.length/3; i++) {
		geom.vertices.push(new THREE.Vector3(150*vertices[3*i],-150*vertices[3*i+1],-150*vertices[3*i+2]));
	}
	
	for (var i=0; i<cubeVertexIndices.length/3; i++) {
		geom.faces.push( new THREE.Face3( cubeVertexIndices[3*i], cubeVertexIndices[3*i+2], cubeVertexIndices[3*i+1] ) );
	}
	
	geom.computeFaceNormals();
	
	var object = new THREE.Mesh( geom, new THREE.MeshNormalMaterial() );
	// var object = new THREE.Mesh( geom, new THREE.MeshNormalMaterial({ opacity: 0.3 }) );
	
	object.rotation.x = Math.PI/2;
	object.rotation.y = -Math.PI;
	object.rotation.z = -Math.PI/2;
	
	return object;
}

function createBlendedMesh() {
	var uniforms = THREE.UniformsUtils.clone( Shaders['cloud'].uniforms );
	uniforms['textureClouds'].texture = texture;

	var geometry = new THREE.SphereGeometry( 150, 200, 200, 0, Math.PI * 2, 0, Math.PI );

	var material = new THREE.ShaderMaterial({
		uniforms: uniforms,
		vertexShader: document.getElementById( 'cloudVertexShader' ).textContent,
		fragmentShader: document.getElementById( 'cloudFragmentShader' ).textContent,
		blending: THREE.AdditiveBlending,
		transparent: true
	});
	cloudMaterial = material;

	mesh = new THREE.Mesh( geometry, material );
	return mesh;
}

function createMap(rad) {
	var u = THREE.UniformsUtils.clone( Shaders['earth'].uniforms );
	u['textureTerrain'].texture = textures[options.map];
	u['nightMultiplier'].value = 0.0;
	// u['cloudShadowAmount'].value = 1.0;
	u['time'].value = 0.0;

	var geometry = new THREE.SphereGeometry( rad, 40, 40, 0, Math.PI * 2, 0, Math.PI);
	var material = new THREE.ShaderMaterial({
		uniforms: u,
		vertexShader: document.getElementById( 'mapVertexShader' ).textContent,
		fragmentShader: document.getElementById( 'mapFragmentShader' ).textContent,
		blending: THREE.AdditiveBlending,
		transparent: true
	});
	mesh = new THREE.Mesh( geometry, material );
	return mesh;
}

function createMap2(rad) {
	var u = THREE.UniformsUtils.clone( Shaders['earth'].uniforms );
	u['textureTerrain'].texture = textures[options.map];
	var geometry = new THREE.SphereGeometry( rad, 40, 40, 0, Math.PI * 2, 0, Math.PI);
	var material = new THREE.ShaderMaterial({
		uniforms: u,
		vertexShader: document.getElementById( 'mapVertexShader' ).textContent,
		fragmentShader: document.getElementById( 'map2FragmentShader' ).textContent,
		blending: THREE.AdditiveBlending,
		transparent: true
	});
	mesh = new THREE.Mesh( geometry, material );
	return mesh;
}

function animate() {

	requestAnimationFrame( animate );

//	checkHurricanes();
	updateGlobeAndCamera();
	renderer.render( scene, camera );
	// effect.render( scene, camera );

	if (show_segments) {
		var progress = video.currentTime / video.duration;
		var nYears = year_segments.length;
		var t = Math.floor(progress*nYears);
		if (t!=current_year_no) {
			globeObject.remove(year_segments[current_year_no][level_no]);
			current_year_no = t;
			globeObject.add(year_segments[current_year_no][level_no]);
		}
	}
}

init();
animate();
