var Shaders = {
	'earth' : {
		uniforms: {
			'textureTerrain': { type: 't', value: 0, texture: null },
			'textureHeight': { type: 't', value: 1, texture: null },
			'textureClouds': { type: 't', value: 2, texture: null },
			'textureNight': {type: 't', value: 3, texture: null },
			'cloudShadowAmount': {type: 'f', value: 0.0 },
			'nightMultiplier': {type: 'f', value: 0.0 },
			'time': {type: 'f', value: 0.0 },
		}
	},
	'cloud' :{
		uniforms: {
			'textureClouds': { type: 't', value: 0, texture: null },
			'height': { type: 'f', value: 0.0 },
			'edgeFade': { type: 'f', value: 0.0 },
		}					
	}
}