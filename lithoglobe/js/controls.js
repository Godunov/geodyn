////////////////////////////////////////
//	ROTATION CONTROLS
////////////////////////////////////////

var mouse = { x: 0, y: 0 };
var mouseOnDown = { x: 0, y: 0 };
var dragging = false;

var PI_HALF = Math.PI / 2;
var clampmax = PI_HALF;
var clampmin = PI_HALF;

function addControlListeners(){
	window.addEventListener( 'mousedown', onMouseDown, false );
	window.addEventListener( 'mouseup', onMouseUp, false );
	window.addEventListener( 'mousewheel', onMouseWheel, false );	
	window.addEventListener( 'mousemove', onMouseMove, false );
	window.addEventListener( 'mouseout', onMouseOut, false );
	window.addEventListener( 'dblclick', onDblClick, false );
}

function onMouseDown( event ) {
	mouseOnDown.x = - event.clientX;
	mouseOnDown.y = event.clientY;

	targetOnDown.x = target.x;
	targetOnDown.y = target.y;

	document.body.style.cursor = 'move';

	dragging = true;
	event.preventDefault();
}

function onMouseUp( event ) {
	document.body.style.cursor = 'auto';
	dragging = false;
}

function onMouseMove( event ) {
	mouse.x = - event.clientX;
	mouse.y = event.clientY;

	if(dragging){
		var zoomDamp = distance / 1000;

		target.x = targetOnDown.x + ( mouse.x - mouseOnDown.x ) * 0.005 * zoomDamp;
		target.y = targetOnDown.y + ( mouse.y - mouseOnDown.y ) * 0.005 * zoomDamp;

//		target.y = target.y > clampmax ? clampmax : target.y;
//		target.y = target.y < - clampmin ? - clampmin : target.y;		
	}
}

function onMouseOut(event) {
	document.body.style.cursor = 'auto';
}

function onDblClick( event ) {
	var mouse_x = ( event.clientX / window.innerWidth ) * 2 - 1;
	var mouse_y = - ( event.clientY / window.innerHeight ) * 2 + 1;
	
	var vector = new THREE.Vector3( mouse_x, mouse_y, 1 );
	projector.unprojectVector( vector, camera );
	var camera_pos = camera.position;
	camera_pos.y = 0;
	camera_pos.z = -camera_pos.z;
	var ray = new THREE.Ray( camera_pos, vector.subSelf( camera_pos ).normalize() );
	// ray.far = -Infinity;
	console.log(camera_pos, mouse_x, mouse_y, ray);
	console.log(meshes[level])

	var intersects2 = ray.intersectObjects( scene.children );
	var intersects = ray.intersectObjects( meshes[level] );
	console.log('intscts:', intersects, intersects2);
	if ( intersects.length > 0 ) {
		var point = intersects[ 0 ].point;
        console.log( point );
    }
}


////////////////////////////////////////
//	ZOOM CONTROLS
////////////////////////////////////////

function onMouseWheel(event) {
	event.preventDefault();
	zoom( event.wheelDeltaY * 0.3 );
}

function zoom( delta ) {
	distanceTarget -= delta;
	constrainZoom();
}

// function onWindowResize( event ) {
	// camera.aspect = window.innerWidth / window.innerHeight;
	// camera.updateProjectionMatrix();

	// renderer.setSize( window.innerWidth, window.innerHeight );
	// timeline.setPosition( window.innerWidth / 2, window.innerHeight - 80 );
// }