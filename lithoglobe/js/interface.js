////////////////////////////////////////
//	HELP HUD
////////////////////////////////////////

var hideHelpHudTime = 8000;		//	milliseconds
var helpHidden = false;

function hideHelp(){
	if( helpHidden )
		return;
	
	helpHidden = true;
	var help = document.getElementById('controlshelp');
	help.style.display = 'none';				
}

//	make the help hud automatically go away after some seconds
setTimeout( function(){
	hideHelp();
}, hideHelpHudTime );	

document.addEventListener( 'mousedown', function(){
	hideHelp();
}, true);	 


////////////////////////////////////////
//	CAMERA STATES
////////////////////////////////////////

var cameraStates = {};
cameraStates['rainforest'] = {
	x: 5.329389819895933,
	y: -0.23190175171385552,
	z: 700,
}
cameraStates['currents'] = {
	x: 5.689352633966016,
	y: 0.30069900877709516,
	z: 700,
}

function setCameraToState( cameraStateName ){
	var state = cameraStates[cameraStateName];
	if( state === undefined )
		return;
	target.x = state.x;
	target.y = state.y;
	distanceTarget = state.z;
}


////////////////////////////////////////
//	CATEGORY SELECTION
////////////////////////////////////////

var buttonHighlightColor = 'rgba(140,140,140,1)';

var VisualState = {
	none: -1,
	storms : 0,
	vegetation : 1,
	currents : 2,
}

var currentlyVisualized = VisualState.none;


//	clears all category button states
function clearCategorySettings(){
	hurricanesEnabled = false;
	$('#iffcontainer').hide();
	globeMaterial.uniforms["textureTerrain"].texture = textures['landocean'];
	// globeMaterial.uniforms['nightMultiplier'].value = 1.0;
	setUIColorState( 'stormselect', '' );
	setUIColorState( 'rainforestselect', '' );
	setUIColorState( 'currentsselect', '' );
}

//	does nothing but finds the element and sets the bg color style
function setUIColorState( id, className ){
	var domElement = document.getElementById(id);
	if( domElement === undefined )
		return;
	// domElement.className = className;
}

//	run when one of the categories is clicked
function pickCategory( category ){
	//	just clear everything...
	clearCategorySettings();	

	if( category == 'hurricanes' ){
		if( currentlyVisualized == VisualState.storms ){
			currentlyVisualized = VisualState.none;
			return;
		}
		currentlyVisualized = VisualState.storms;
//		hurricanesEnabled = true;
		hurricanesEnabled = false;
//		$('#iffcontainer').show();
		$('#iffcontainer').hide();
		globeMaterial.uniforms["textureTerrain"].texture = textures['landocean'];
		// globeMaterial.uniforms['nightMultiplier'].value = 1.0;
		setUIColorState( 'stormselect', 'selected' );
		setUIColorState( 'rainforestselect', '' );
		setUIColorState( 'currentsselect', '' );
//		$('#stormmarkers').show();
		$('#stormmarkers').hide();
	}
	else if( category == 'rainforest' ){
		if( currentlyVisualized == VisualState.vegetation ){
			currentlyVisualized = VisualState.none;
			return;
		}
		currentlyVisualized = VisualState.vegetation;
		globeMaterial.uniforms["textureTerrain"].texture = textures['vegetation'];		
		// globeMaterial.uniforms['nightMultiplier'].value = 1.0;
		// setCameraToState( 'rainforest' );
		setUIColorState( 'rainforestselect', 'selected' );
		setUIColorState( 'stormselect', '' );
		setUIColorState( 'currentsselect', '' );
		$('#stormmarkers').hide();
	}
	else if( category == 'currents' ){
		if( currentlyVisualized == VisualState.currents ){
			currentlyVisualized = VisualState.none;
			return;
		}
		currentlyVisualized = VisualState.currents;
		globeMaterial.uniforms["textureTerrain"].texture = textures['currents'];
		// globeMaterial.uniforms['nightMultiplier'].value = 0.0;
		// setCameraToState( 'currents' );
		setUIColorState( 'currentsselect', 'selected' );
		setUIColorState( 'stormselect', '' );
		setUIColorState( 'rainforestselect', '' );
		$('#stormmarkers').hide();
	}
}


////////////////////////////////////////
//	EXTENDED INFO UI
////////////////////////////////////////

function toggleExtended(){
	var extendedInfo = document.getElementById('extendedinfo');
	var aboutLink = document.getElementById('about-link');

	if( extendedInfo.style.visibility === 'visible' ){
		extendedInfo.style.visibility = 'hidden';
		// aboutLink.style['background-color'] = '#1d1d1d';
	}
	else{
		extendedInfo.style.visibility = 'visible';
		// aboutLink.style['background-color'] = 'rgba(255, 255, 255, .35)';
	}
}
