close all
clear all
clc

folder = '../data/igrf11coeffs';

width = 512;
height = 256;

files = dir([folder '/*.mat']);

filename = [folder '/' files(1).name];
load(filename)
data = zeros(size(harmonics,1), size(harmonics,2), size(harmonics,3), length(files));
data(:,:,:,1) = harmonics;

for file_no=2:length(files)
    filename = [folder '/' files(file_no).name];

    [pathstr, name, ext] = fileparts(filename);
    levels_dir = [pathstr '_levels_spectra'];
    mkdir(levels_dir);

    load(filename)
    data(:,:,:,file_no) = harmonics;
end

ntimes = size(data,4);
[nx ny nz nsp] = size(data);
normalized = Normalize(data, 4);
for z_level=1:nz
    RGB = Spectra2RGB(reshape(normalized(:,:,z_level,:),nx,ny,nsp));

    out = [levels_dir '/' num2str(z_level) '.png']
    imwrite(2*RGB, out, 'png');
end

time_step = 5;
ntimes = size(data,4);
for t=1:ntimes-time_step
    tmp = data;
    spectra_cube = Normalize(tmp(:,:,:,t:t+time_step), 4);
%     spectra_cube = data(:,:,:,t:t+time_step);

    [nx ny nz nsp] = size(spectra_cube);
    for z_level=1:nz;
        framedir = [levels_dir '/time.step.' num2str(time_step) '/' num2str(z_level)];
        mkdir(framedir);
        
        RGB = Spectra2RGB(reshape(spectra_cube(:,:,z_level,:),nx,ny,nsp));
        
        RGB = imresize(RGB, [height width]);
        out = [framedir '/' num2str(t) '.png']
        imwrite(2*RGB, out, 'png');
        
%         imshow(2*RGB,[])
%         title(['time: ' num2str(t) '-' num2str(t+time_step)])
%         pause
    end
end
