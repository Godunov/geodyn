function [] = mkframes()
folder = '../data/igrf11coeffs';

width = 512;
height = 256;

igrf11_values = '-1e6 -1e5 -1e4 0 1e4 1e5 1e6';
vals = str2num(igrf11_values);

igrf11_colors = '000000 0000FF 00FFFF FFFFFF FFFF00 FF0000 000000';
igrf11_colors = igrf11_colors(igrf11_colors~=' ');
colors = zeros(length(igrf11_colors)/6,3);
for i=1:length(igrf11_colors)/6
    colors(i,1) = hex2dec(igrf11_colors((i-1)*6+1:(i-1)*6+2));
    colors(i,2) = hex2dec(igrf11_colors((i-1)*6+3:(i-1)*6+4));
    colors(i,3) = hex2dec(igrf11_colors((i-1)*6+5:(i-1)*6+6));
end

files = dir([folder '/*.mat']);
for file_no=1:length(files)
    filename = [folder '/' files(file_no).name];

    [pathstr, name, ext] = fileparts(filename);
    levels_dir = [pathstr '_levels'];
    mkdir(levels_dir);

    load(filename)
    for i=1:size(harmonics,3)
        h = flipud(harmonics(:,:,i));
        h = circshift(h, [0 180]);
        
%         maxh = max(h(:));
%         minh = min(h(:));
%         h = uint8((254*(h-minh)/(maxh-minh))+1);
% 
%         RGB = label2rgb(h);

        nel1 = size(h,1);
        nel2 = size(h,2);
        RGB = zeros(nel1, nel2, 3);
        for c1=1:nel1
            for c2=1:nel2
                RGB(c1, c2, :) = getColorFromValue(h(c1, c2), vals, colors)/255;
            end
        end
        RGB = imresize(RGB, [height width]);
        
%         imshow(RGB)
%         axis image
%         pause
        
        framedir = [levels_dir '/' num2str(i)];
        mkdir(framedir);
        imwrite(RGB, [framedir '/' name '.png'], 'png');
    end
end

function [color] = getColorFromValue(value, values, colors)
ind = find(values>=value);
if isempty(ind)
    n = length(values);
else
    n = ind(1)-1;
end

i1 = 0;
i2 = 0;
% if (n >= 0)
%     color = colors(n+1,:);
%     return
% else
    if (n >= length(values))
        color = colors(length(values),:);
        return
    else
        if (n == 0)
            color = colors(1,:);
            return
        else
            i2 = n;
            i1 = n - 1;
        end
    end
% end

c1 = colors(i1+1,:);
c2 = colors(i2+1,:);

x = value;
x1 = values(i1+1);
x2 = values(i2+1);
f1 = colors(i1+1,:);
f2 = colors(i2+1,:);
        
if (x == x1)
    color = f1;
    return 
else
    if (x == x2)
        color = f2;
        return
    else
        color = (((x2 - x) * c1 + (x - x1) * c2) / (x2 - x1));
        return
    end
end
