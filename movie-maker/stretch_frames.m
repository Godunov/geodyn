close all
clear all
clc

folder = '../data/igrf11coeffs_levels';
resfolder = '../data/igrf11coeffs_levels_frames';
framestretch = 4;
debug = false;

% folder = '../data/igrf11coeffs_levels_spectra/time.step.5';
% resfolder = '../data/igrf11coeffs_levels_frames_spectra/time.step.5';
% framestretch = 4;
% debug = false;

% folder = '../data/s362d/s362d';
% resfolder = '../data/s362d/s362d_frames';
% framestretch = 4;
% debug = false;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mkdir(resfolder)

folders = dir(folder);

for i=1:length(folders)
    f = folders(i);
    in = [folder '/' f.name];
    out = [resfolder '/' f.name]
    StretchFrames(in, out, framestretch, debug);
end