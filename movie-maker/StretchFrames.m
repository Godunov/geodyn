function [] = StretchFrames(folder, resfolder, framestretch, debug)
% Stretch frames by alpha blending

if nargin < 4
    debug = false;
end

if ~isdir(folder)
    return;
end

list = dir([folder '/*.png']);
fnames = {list.name};
str  = sprintf('%s#', fnames{:});
num  = sscanf(str, '%d.png#');
[dummy, index] = sort(num);
fnames = fnames(index);

if isempty(fnames)
    return;
end

mkdir(resfolder)

first = double(imread([folder '/' fnames{1}]));

count = 0;
for i=2:length(fnames)
    second = double(imread([folder '/' fnames{i}]));
    
    for j=1:framestretch
        alpha = 1-(j-1)/framestretch;

        frame = alpha*first+(1-alpha)*second;
        
        frame_fname = '0000';
        c = num2str(count);
        frame_fname(length(frame_fname)-length(c)+1:end) = c;
        
        if debug
            subplot(3,1,1)
            imshow(uint8(first))
            subplot(3,1,2)
            imshow(uint8(second))
            subplot(3,1,3)
            imshow(uint8(frame))
            pause;
        else
            imwrite(uint8(frame), [resfolder '/' frame_fname '.jpg'],'jpg')
        end
        
        count = count+1;
    end
    
    first = second;
end








