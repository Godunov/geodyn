function [ res ] = phi_geocentric( phi, a, b )

    res = atan((b/a)^2*tan(phi));
    
end