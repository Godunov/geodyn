function [ Cnm, Snm ] = read_coef (data)

Cnm = zeros(180,180);
Snm = zeros(180,180);

for i=1:16471
    n = data(i,1);
    m = data(i,2);
    c = data(i,3);
    s = data(i,4);
    Cnm(n+1,m+1) = c;
    Snm(n+1,m+1) = s;
end