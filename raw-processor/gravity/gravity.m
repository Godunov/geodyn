function [ res ] = gravity( phi, a, b, GM, omega )
    
    m = omega^2*a^2*b/GM;
    E = sqrt(a^2-b^2);
    e_prim = E/b;
    e = E/a;
    gamma_a = GM/(a*b)*(1-3/2*m-3/14*e_prim*m);
    gamma_b = GM/a^2*(1+m+3/7*e_prim*m);
    k = (b*gamma_b - a*gamma_a)/(a*gamma_a);
    res = gamma_a*(1+k*sin(phi)^2)/sqrt(1-e^2*sin(phi)^2);
    
end