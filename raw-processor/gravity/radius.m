function [ res ] = radius( phi, a, f )
    
    res = a*(1 - f * sin(phi)^2);

end

