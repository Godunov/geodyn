function [ data ] = calculate (Cnm, Snm)

data = zeros(181,361,1);
GM = 0.3986004418D+15;
omega = 7.292115D-5;
a = 6378137.0;
f = 1.0/298.2572235630;
b = a*(1-f);
n_max = 80;

C_prim = Cnm;

J2=0.108262982131D-2;
J4=-.237091120053D-05;
J6=0.608346498882D-8;
J8=-0.142681087920D-10;
J10=0.121439275882D-13;
      
C_prim(3,1) = Cnm(3,1)+J2/sqrt(4+1);
C_prim(5,1) = Cnm(5,1)+J4/sqrt(8+1);
C_prim(7,1) = Cnm(7,1)+J6/sqrt(12+1);
C_prim(9,1) = Cnm(9,1)+J8/sqrt(16+1);
C_prim(11,1) = Cnm(11,1)+J10/sqrt(20+1);

for lat = -90:90
    lat
    phi = pi*lat/180;
    r_phi = radius(phi,a,f);
    g_phi = gravity(phi, a, b, GM, omega);
    phi_prim = phi_geocentric(phi,a,b);
    for lon = 0:360
        lambda = pi*lon/180;
        data(lat+90+1,lon+1,1) = single_point(lambda,phi_prim,r_phi,g_phi,a,GM,C_prim,Snm,n_max);
    end
end


