function [ res ] = single_point( lambda, phi_prim, r_phi, g_phi, a, GM, Cnm, Snm, n_max )

sum1 = 0;
%csum = 0;
for n = 2:n_max
    Pn = legendre(n,sin(phi_prim),'norm');
    sum2 = Cnm(n+1,1)*Pn(1)*sqrt(2);
    for m = 1:n
        sum2 = sum2 + (Cnm(n+1,m+1)*cos(m*lambda)+Snm(n+1,m+1)*sin(m*lambda))*Pn(m+1)*2;
        %csum = csum + (CCnm(n+1,m+1)*cos(m*lambda)+CSnm(n+1,m+1)*sin(m*lambda))*Pn(m+1)*norm;
    end
    sum1 = sum1 + ((a/r_phi)^n) * sum2;  %*   g_phi*n/r_phi;
end
res = sum1*GM/(g_phi*r_phi); %+ csum/100.0 - 0.53;