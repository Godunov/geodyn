function [C, S] = readcoeff(filename,layer)

fid = fopen(filename,'r');
n = fscanf(fid, '%d', 1);

for i=1:n
    z = fscanf(fid, '%g', 1);
    lmax = fscanf(fid, '%d', 1);

    if i==layer
        C = zeros(lmax);
        S = zeros(lmax);
        for l=0:lmax
            for m=0:l
                [a,b] = fscanf(fid, '%g %g', 2);
                C(l+1,m+1) = a;
                S(l+1,m+1) = b;
            end
        end
    else
        for l=0:lmax
            for m=0:l
                fscanf(fid, '%g %g', 2);
            end
        end
    end
    
end

fclose(fid);
