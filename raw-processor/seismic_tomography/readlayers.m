function [Z] = readlayers(filename)

fid = fopen(filename,'r');
n = fscanf(fid, '%d', 1);
Z = zeros(n,1);
for i=1:n
    Z(i) = fscanf(fid, '%g', 1);
    lmax = fscanf(fid, '%d', 1);
    for l=0:lmax
        for m=0:l
            fscanf(fid, '%g %g', 2);
        end
    end
end

fclose(fid);
