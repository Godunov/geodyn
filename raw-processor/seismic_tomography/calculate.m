function [ data ] = calculate (C, S)

data = zeros(181,361);
n_max = 31;



for lat = -90:90
    %lat
    phi = pi*lat/180;
    for lon = 0:360
        lambda = pi*lon/180 - pi;
        data(lat+90+1,lon+1) = single_point(lambda,phi,C,S,n_max);
    end
end


