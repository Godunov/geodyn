function [ res ] = single_point( lambda, phi, C, S, n_max )

sum1 = 0;
norm = sqrt(4*pi);
for n = 0:n_max
    Pn = legendre(n,sin(phi),'norm');
    sum2 = 0;
    for m = 1:n
        sum2 = sum2 + (C(n+1,m+1)*cos(m*lambda)+S(n+1,m+1)*sin(m*lambda))*Pn(m+1);
        
    end
    sum1 = sum1 + C(n+1,1)*Pn(1) + sqrt(2)*sum2; 
end
res = sum1/norm;