function value = Atom( n,m,fi,psi,coeff, nmax, lezh,level )
%ATOM Summary of this function goes here
%   Detailed explanation goes here

indexN=n;
AindexM=m+nmax+1;
BindexM=nmax+1-m;
Anm=coeff(indexN, AindexM);
Bnm=coeff(indexN, BindexM);
%lezh=getLezh(n,m,cos(psi));
%lezh=legendre(n, cos(psi), 'norm');
value=(n+1)*((6371/(6371-level))^(n+2))*(Anm*cos(m*fi)+Bnm*sin(m*fi))*lezh;

end

