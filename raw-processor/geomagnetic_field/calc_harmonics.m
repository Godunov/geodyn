filename = '../../data/igrf11coeffs.xls';

[num txt raw] = xlsread(filename);
data = zeros(195,27);
for i=5:size(raw,1)
    gh = raw(i,1);
    data(i-4,1) = gh{1}=='h';
    for j=2:27
        val = raw(i,j);
        data(i-4,j) = val{1};
    end
end

[pathstr, name, ext] = fileparts(filename);
harmonics_fpath = [pathstr '/' name];
mkdir(harmonics_fpath);

for i=4:26
    coeff = coeffread(data,i);
    harmonics = Harmonics(coeff);
    year = raw(4,i);
    save([harmonics_fpath '/' num2str(year{1}) '.mat'], 'harmonics')
end
