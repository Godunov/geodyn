function value = SingleHarmonicValue( coeff, fi, psi, nmax ,level)
%SINGLEHARMONICVALUE Summary of this function goes here
%   Detailed explanation goes here


res=0;
for n=1:nmax
    lezh = legendre(n, cos(psi),'sch');
    for m=0:n
        res=res+Atom(n,m,fi,psi,coeff,nmax,lezh(m+1),level);
    end
end
value=res;

