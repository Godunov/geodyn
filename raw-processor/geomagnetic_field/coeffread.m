function [res] = coeffread(data, yearcol)

coeff = zeros(13,27);
for i=1:195
    f = data(i,1);
    n = data(i,2);
    m = data(i,3);
    gh = data(i,yearcol);
    if f==0 
        coeff(n,m+14) = gh;
    else
        coeff(n,14-m) = gh;
    end
end

res=coeff;