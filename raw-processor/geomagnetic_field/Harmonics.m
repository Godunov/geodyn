function [res] = Harmonics(coeff)

n=13;

% [data,lat,lon,time]=getNcepDataTemperature('1982-01-01T00:00:00UTC','2006-12-31T23:59:59UTC',55.0,55.0,37.5,37.5,'ns1/air0', 'arca7.wdcb.ru:1433');
% timeZero=datenum(1970,1,1);
% timeD=time/24;
% timeD=timeD+timeZero;

%load coeff.mat; 

% per 1 degree
%nlat=181;
%nlon=361;
values=zeros(181, 361, 11);

i=1;
for level=0:300:3000
    disp(['progress: ' num2str(100*level/3000)])
    for lat=-90:90
        psi=pi*(90-lat)/180;
        for lon = 0:360
            fi=2*pi*lon/360;
            values(lat+90+1,lon+1,i)= SingleHarmonicValue(coeff, fi, psi, n, level);
        end
    end
    i = i+1;
end

res=values;



% figure
% plot(sunN);

% nt=size(data,1);
% ny=floor(nt/25);
% adata=zeros(25, 1);
% for y = 1:25
%     adata(y)=sum(data(1+ny*(y-1):ny*y))/ny;
% end;
% nt2=size(sunN,1);
% ny2=floor(nt2/25);
% AsunN=zeros(25, 1);
% for y = 1:25
%     AsunN(y)=sum(sunN(1+ny2*(y-1):ny2*y))/ny2;
% end;
% 
% figure;
% plot([normalizePA(adata), normalizePA(AsunN)]);
% 
% % figure;
% % plot(AsunN);
% % datetick('x','keepticks');